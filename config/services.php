<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Push Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing access credentials for the various push services
    | that your web application can communicate with. You can override these
    | settings by editing your app's config/services.php file as needed.
    |
    */

    'pushover' => [
        'user_key' => env('PUSHOVER_USER_KEY'),
        'app_key' => env('PUSHOVER_APP_KEY'),
        'title' => env('APP_TITLE', 'Your Website'),
    ],

];
