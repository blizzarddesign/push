<?php

namespace Blizzarddesign\Push\Services;

//use Log;
use Illuminate\Log\Writer as Log;
use Blizzarddesign\Push\Exceptions\PushException;

class Pushover
{
    const URL = 'https://api.pushover.net/1/messages.json';

    private $user_key;
    private $app_key;
    private $message;
    private $title;
    private $icon;
    private $priority = 0;
    private $sound = 'pushover';
    private $devices;
    private $message_url;
    private $message_url_title;
    private $timestamp;
    private $retry = 300; // 5 minutes
    private $expire = 3600; // 1 hour
    private $callback_url;

    protected $log;

    public function __construct(Log $log)
    {
        $this->log = $log;
        $this->user_key = config('services.pushover.user_key');
        $this->app_key = config('services.pushover.app_key');
        $this->title(config('services.pushover.title'));
    }

    // easy notification presets
    public function info($message)
    {
        return $this->message($message)->send();
    }

    public function error($message)
    {
        return $this->message($message)->icon('❗️')->priority(1)->sound('falling')->send();
    }

    public function critical($message, $callback_url = '')
    {
        if ( ! empty($callback_url)) {
            $this->callbackUrl($callback_url);
        }
        return $this->message($message)->icon('🔥')->priority(2)->sound('alien')->send();
    }

    public function cash($message)
    {
        return $this->message($message)->icon('💰')->sound('cashregister')->send();
    }

    // build-your-own notification setters
    public function message($message)
    {
        $this->message = (string)$message;
        return $this;
    }

    public function title($title)
    {
        $this->title = (string)$title;
        return $this;
    }

    public function icon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    public function priority($priority)
    {
        $this->priority = (int)$priority;
        return $this;
    }

    public function sound($sound)
    {
        $this->sound = (string)$sound;
        return $this;
    }

    public function devices(array $devices)
    {
        $this->devices = implode(',', $devices);
        return $this;
    }

    public function url($url, $title = null)
    {
        if ( ! empty($title)) {
            $this->message_url_title = (string)$title;
        }
        $this->message_url = (string)$url;
        return $this;
    }

    public function timestamp($timestamp)
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    public function retry($retry_seconds, $expire_seconds)
    {
        $this->retry = (int)$retry_seconds;
        $this->expire = (int)$expire_seconds;
        return $this;
    }

    public function callbackUrl($url)
    {
        $this->callback_url = (string)$url;
        return $this;
    }

    // execute the call
    public function send()
    {
        // verify configuration
        $this->validate();

        // send to pushover api
        $response = $this->sendApiCall();

        return ($response ? $this->returnResponse($response) : false);
    }

    // execution private helpers
    private function validate()
    {
        if (empty($this->user_key) || empty($this->app_key)) {
            throw new PushException('Push services are not configured correctly.');
        }
        if (empty($this->message)) {
            throw new PushException('Push notifications require a message to be set.');
        }

        return true;
    }

    private function sendApiCall()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->fields());
        $response = curl_exec($curl);

        // check for http response errors
        $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($http_code != 200 && $http_code != 400) {
            $this->logError('Pushover notification failed.', ['http_code' => $http_code]);
            return false;
        }

        return $response;
    }

    private function fields()
    {
        $fields = [
            // required
            'token' => $this->app_key,
            'user' => $this->user_key,
            'message' => $this->message,
            // optional
            'title' => ($this->icon ? $this->icon . ' ': '') . $this->title,
            'priority' => $this->priority,
            'sound' => $this->sound,
            'device' => $this->devices,
            'url' => $this->message_url,
            'url_title' => $this->message_url_title,
            'timestamp' => $this->timestamp,
            'html' => 1
        ];

        // emergency priority notification optional fields
        if ($this->priority == 2) {
            $fields['retry'] = $this->retry;
            $fields['expire'] = $this->expire;
            if ( ! empty($this->callback_url)) {
                $fields['callback'] = $this->callback_url;
            }
        }

        return $fields;
    }

    private function returnResponse($response)
    {
        $response = json_decode($response);

        if ($response->status == 0) {
            // error: problem reported from pushover
            $this->logError('Pushover notification failed.', $response->errors);
            return false;
        }

        // return request token if emergency priority
        if ($this->priority == 2) {
            return $response->request;
        }

        return ($response->status == 1) ? true : false;
    }

    private function logError($summary, $errors = [])
    {
        $this->log->error($summary, [
            'message_body' => $this->message,
            'errors' => $errors
        ]);
    }
}
