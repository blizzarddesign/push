<?php

namespace Blizzarddesign\Push;

use Illuminate\Support\ServiceProvider;
use Blizzarddesign\Push\Services\Pushover;

class PushServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // merge package config into framework's service.php config
        $this->mergeConfigFrom(
            __DIR__ . '/../config/services.php', 'services'
        );
    }

    public function register()
    {
        // bind package to framework
        $this->app->bind('blizzarddesign-push', Pushover::class);
    }
}
